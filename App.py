
"""
---------- Dedicated to Mary our mother in spirit, and to her son Jesus our brother in spirit.
---------- Just like Adam and Eve. A new beginning...
"""

import sys
sys.path.append('./libs')
import xml2gui as g
import math
import datetime


template = ""


def show_time():
    global template
    
    now = datetime.datetime.now()
    hour = now.hour
    min = now.minute
    sec = now.second
    usec = now.microsecond
    
    W = g.mcxml_get_width("imgWidget")
    H = g.mcxml_get_height("imgWidget")
    xscale = W / 512.0
    yscale = H / 512.0
    
    sec_deg  = (sec/60.0) * 360 + ((usec/1000000.0) * 1/60 * 360.0)  # let usecs influence sec hand
    min_deg  = (min/60.0) * 360 + ((sec/60.0) * 1/60 * 360)  # let seconds influence minute hand
    hour_deg = ((hour/12.0) * 360) + ((min/60.0) * 5/60 * 360.0)  # let minutes influence hour hand
    hour_deg = math.fmod(hour_deg, 360)
    min_deg  = math.fmod(min_deg,  360)
    sec_deg  = math.fmod(sec_deg,  360)
    s = template.format(W, H, xscale, yscale, hour_deg, min_deg, sec_deg)
    g.mcxml_set_src("imgWidget", s)
    g.mcxml_set()

    
def listener_started():
    show_time()
    
    
def listener_repeat():
    show_time()


if __name__ == '__main__':
    xml = open('app.xml', mode='r', encoding='utf-8').read()
    template = open('template.txt', mode='r', encoding='utf-8').read()
    g.mcxml_started(listener_started)
    g.mcxml_repeat(listener_repeat, 0.1)
    g.mcxml_loop(xml)
    
